<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\EventsUsersTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\EventsUsersTable Test Case
 */
class EventsUsersTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\EventsUsersTable
     */
    public $EventsUsers;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.events_users',
        'app.users',
        'app.events'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('EventsUsers') ? [] : ['className' => 'App\Model\Table\EventsUsersTable'];
        $this->EventsUsers = TableRegistry::get('EventsUsers', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->EventsUsers);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
