<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit User'), ['action' => 'edit', $user->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete User'), ['action' => 'delete', $user->id], ['confirm' => __('Are you sure you want to delete # {0}?', $user->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Users'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New User'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Events Users'), ['controller' => 'EventsUsers', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Events User'), ['controller' => 'EventsUsers', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="users view large-9 medium-8 columns content">
    <h3><?= h($user->name) ?></h3>
    <table class="vertical-table">
        <tr>
            <th><?= __('Name') ?></th>
            <td><?= h($user->name) ?></td>
        </tr>
        <tr>
            <th><?= __('Email') ?></th>
            <td><?= h($user->email) ?></td>
        </tr>
        <tr>
            <th><?= __('Id') ?></th>
            <td><?= $this->Number->format($user->id) ?></td>
        </tr>
        <tr>
            <th><?= __('Created') ?></th>
            <td><?= h($user->created) ?></td>
        </tr>
        <tr>
            <th><?= __('Modified') ?></th>
            <td><?= h($user->modified) ?></td>
        </tr>
    </table>
    <div class="related">
        <h4><?= __('Related Events Users') ?></h4>
        <?php if (!empty($user->events_users)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th><?= __('Id') ?></th>
                <th><?= __('User Id') ?></th>
                <th><?= __('Event Id') ?></th>
                <th><?= __('Created') ?></th>
                <th><?= __('Modified') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($user->events_users as $eventsUsers): ?>
            <tr>
                <td><?= h($eventsUsers->id) ?></td>
                <td><?= h($eventsUsers->user_id) ?></td>
                <td><?= h($eventsUsers->event_id) ?></td>
                <td><?= h($eventsUsers->created) ?></td>
                <td><?= h($eventsUsers->modified) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'EventsUsers', 'action' => 'view', $eventsUsers->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'EventsUsers', 'action' => 'edit', $eventsUsers->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'EventsUsers', 'action' => 'delete', $eventsUsers->id], ['confirm' => __('Are you sure you want to delete # {0}?', $eventsUsers->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
</div>
